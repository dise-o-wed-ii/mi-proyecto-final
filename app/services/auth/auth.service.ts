

import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthService implements CanActivate {

	constructor(private router: Router) {}

	// Implementación del método `canActivate` requerido por la interfaz CanActivate
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		let url: string = state.url;

		// Verifica si los datos del usuario están almacenados en el almacenamiento local
		if (localStorage.getItem('userData')) {
			return true; // Permite la navegación a la ruta solicitada
		} else {
			this.router.navigate(['/login']); // Redirige a la página de inicio de sesión si no se encuentra autenticado
			return false; // Impide la navegación a la ruta solicitada
		}
	}
}
