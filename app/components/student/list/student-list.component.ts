import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';


// Servicios
import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css'],
  animations: [routerTransition()],
  host: { '[@routerTransition]': '' }
})

export class StudentListComponent implements OnInit {
  studentList: any;
  studentListData: any;
  filterData: string = '';

  constructor(private studentService: StudentService, private toastr: ToastrService) { }

  // Llamar a la función de lista de estudiantes al cargar la página
  ngOnInit() {
    this.getStudentList();
  }

  // Obtener la lista de estudiantes desde los servicios
  getStudentList() {
    let studentList = this.studentService.getAllStudents();
    this.success(studentList);
  }

  // Éxito en la obtención de la lista de estudiantes
  success(data: any) {
    this.studentListData = data.data;
    for (var i = 0; i < this.studentListData.length; i++) {
      this.studentListData[i].name = this.studentListData[i].dni + ' ' + this.studentListData[i].nombre + ' ' + this.studentListData[i].apellido + ' ' + this.studentListData[i].carrera;
    }
  }

  deleteStudent(index: number) {
    // ... (código existente para eliminar estudiantes)
  }

  // Función para exportar la lista de estudiantes a un archivo Excel
  exportToExcel() {
	const data = [
	  ['DNI', 'Nombre', 'Apellido', 'Correo', 'Carrera', 'Teléfono'], // Encabezados de columna
	  ...this.studentListData.map((student: { dni: any; nombre: any; apellido: any; carrera: any; correo: any; telefono: any; }) => [student.dni, student.nombre, student.apellido, student.correo,student.carrera,  student.telefono])
	];
  
	const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
	const wb: XLSX.WorkBook = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(wb, ws, 'Estudiantes');
  
	// Guardar el archivo Excel
	XLSX.writeFile(wb, 'estudiantes.xlsx');
  }
  
  
  
}
