/**
 * Created By : Sangwin Gawande (https://sangw.in)
 */
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';

// Servicios
import { ValidationService } from '../../../services/config/config.service';
import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-student-add',
	templateUrl: './student-add.component.html',
	styleUrls: ['./student-add.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class StudentAddComponent implements OnInit {
	// Crear studentAddForm de tipo FormGroup 
	studentAddForm: FormGroup;
	index: any;

	constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private studentService: StudentService, private toastr: ToastrService) {

		// Verificar los parámetros de la ruta
		this.route.params.subscribe(params => {
			this.index = params['id'];
			// Verificar si el ID existe en la ruta y llamar a los métodos de actualización o adición en consecuencia
			if (this.index && this.index != null && this.index != undefined) {
				this.getStudentDetails(this.index);
			} else {
				this.createForm(null);
			}
		});
	}

	ngOnInit() {
	}

     // Enviar el formulario de detalles del estudiant
	doRegister() {
		if (this.index && this.index != null && this.index != undefined) {
			this.studentAddForm.value.id = this.index
		} else {
			this.index = null;
		}
		let studentRegister = this.studentService.doRegisterStudent(this.studentAddForm.value, this.index);
		if (studentRegister) {
			if (studentRegister.code == 200) {
				this.toastr.success(studentRegister.message, "Exito");
				this.router.navigate(['/']);
			} else {
				this.toastr.error(studentRegister.message, "Fallido");
			}
		}
	}

	// Si este es un formulario de actualización, obtén los detalles del estudiante y actualiza el formulario
	getStudentDetails(index: number) {
		let studentDetail = this.studentService.getStudentDetails(index);
		this.createForm(studentDetail);
	}

    // Si esta es una solicitud de actualización, llena automáticamente el formulario
    createForm(data: any) {
		if (data == null) {
			this.studentAddForm = this.formBuilder.group({
				dni: ['', [Validators.required, ValidationService.checkLimit(5000000, 9999999)]],
				nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
	            apellido: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
	            telefono: ['', [Validators.required, ValidationService.checkLimit(5000000000, 9999999999)]],
				carrera:['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
	            correo: ['', [Validators.required, ValidationService.emailValidator]]
			});
		} else {
			this.studentAddForm = this.formBuilder.group({
				dni: ['', [Validators.required, ValidationService.checkLimit(5000000, 9999999)]],
				nombre: [data.studentData.nombre, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
		        apellido: [data.studentData.apellido, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
		        telefono: [data.studentData.telefono, [Validators.required, ValidationService.checkLimit(5000000000, 9999999999)]],
				carrera:['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
		        correo: [data.studentData.correo, [Validators.required, ValidationService.emailValidator]]
			});
		}
	}

}

