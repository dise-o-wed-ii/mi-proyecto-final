
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

// Servicios
import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

@Component({
	selector: 'app-student-details',
	templateUrl: './student-details.component.html',
	styleUrls: ['./student-details.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class StudentDetailsComponent implements OnInit {
	index: any;
	studentDetail: any;

	constructor(private router: Router, private route: ActivatedRoute, private studentService: StudentService, private toastr: ToastrService) {
		// Obtener el número de índice del detalle del estudiante enviado en los parámetros
		this.route.params.subscribe(params => {
			this.index = params['id'];
			if (this.index && this.index != null && this.index != undefined) {
				this.getStudentDetails(this.index);
			}
		});
	}

	ngOnInit() {
	}

	// Obtener detalles del estudiante
	getStudentDetails(index: number) {
		let getStudentDetail = this.studentService.getStudentDetails(index);
		if (getStudentDetail) {
			this.studentDetail = getStudentDetail.studentData;
			this.toastr.success(getStudentDetail.message, "Éxito");
		}
	}
}

/**
 * Creado por: Sangwin Gawande (https://sangw.in)
 */
