import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter' // Nombre del filtro que se utilizará en las plantillas HTML
})
export class FilterPipe implements PipeTransform {

  // Implementación del método `transform` requerido por la interfaz PipeTransform
  transform(value: any, args?: any): any {
    if (args != undefined && args != null && args != '') {
      // Filtrar el array `value` para incluir solo elementos cuyo nombre contenga `args`
      return value.filter((data: any) => (data.name.toLowerCase()).indexOf(args.toLowerCase()) > -1);
    }
    // Si no se proporciona `args` o está vacío, devuelve el array original sin filtrar
    return value;
  }
}
